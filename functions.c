#include <stdio.h>
#include <search.h>
#include <string.h>
#include "header.h"
#include "sqlite3.h"

void roskienKeruu(void){

    while( fgetc(stdin) != '\n');
    ;
}

double lueReaaliluku(void){

    double luku;
    char mki;
    int status;

    printf("Syötä talletettava summa --> ");

    while((status = scanf("%lf%c", &luku, &mki))==0  || (2 == status && mki != '\n') ){
        roskienKeruu();
        printf("Annoit virheellisen syötteen. Yritä uudelleen > ");
    }

    return luku;
}

int lueKokonaisluku(void){

    int luku;
    char mki;
    int status;

    while((status = scanf("%d%c", &luku, &mki))==0  || (2 == status && mki != '\n') ){
        roskienKeruu();
        printf("Annoit virheellisen syötteen. Yritä uudelleen > ");
    }

    return luku;
}
/* Valikkofunktio
 * Funktio valikon näyttämiselle.
 *
 * Parametrit:
 * -tilinSaldo (double): main-funktiolta saatu tilin saldo
 *
 * Paluuarvo: tilinSaldo (double), rahannosto- tai rahantalletus-funktiolta saatu päivitetty tilin saldo
 */

void valikko (double * saldoPtr, char *PIN_koodi_kortti, char *kayttajanTiliNro) {

    int menuValinta;
    char vaaraValintaMenussa;

    do {
        printf("Valitse toiminto kirjoittamalla sitä vastaava numero ja painamalla ENTER\n\n");
        printf("1. Nosta rahaa\n2. Näytä saldo\n3. Talleta rahaa\n4. Kirjaudu ulos\n\n");
        printf("Valintasi --> ");
        menuValinta=lueKokonaisluku();

        switch (menuValinta) {
            case 1:
                rahanNosto(saldoPtr, PIN_koodi_kortti, kayttajanTiliNro);
                vaaraValintaMenussa=0;
                break;
            case 2:
                saldonNaytto(saldoPtr);
                vaaraValintaMenussa=0;
                break;
            case 3:
                rahanTalletus(saldoPtr, PIN_koodi_kortti, kayttajanTiliNro);
                vaaraValintaMenussa=0;
                break;
            case 4:
                printf("Kirjaudutaan ulos. Muista poistaa kortti lukijasta.\n");
                break;
            default:
                printf("Valintaasi vastaavaa toimintoa ei löytynyt. Yritä uudelleen.\n\n\n");
                vaaraValintaMenussa=1;
                break;
        }
    } while (vaaraValintaMenussa==1);

}
void updateSaldo ( double *saldoPtr, char *PIN_koodi_kortti, char *kayttajanTiliNro ) {
    FILE *tiliTiedosto;

    tiliTiedosto = fopen(kayttajanTiliNro, "w+");
    if (tiliTiedosto == NULL)
        printf("Tiedoston luku ei onnistunut..");
    fputs(PIN_koodi_kortti, tiliTiedosto);
    fputs("\n", tiliTiedosto);
    fprintf(tiliTiedosto, "%lf", *saldoPtr);

    fclose(tiliTiedosto);
}

/* Saldonnäyttömoduuli
 Toiminnallisuudet saldon näyttämiseen. Funktio tulostaa kehotteen ja käyttäjän vastaus tallennetaan int-tyyppiseen muuttujaan.
 Saldonnäyttömoduulia kutsutaan käyttäjän niin halutessa sekä main-, rahannosto- että rahantalletus-moduuleista.

 Parametrit:
 -tilinSaldo (double): main-, rahannosto- tai rahantalletus-funktiolta saatu tilin saldo

 Ei paluuarvoja

*/

void saldonNaytto (double *tilinSaldo) {

    int vaaraValintaMenussa = 1;
    int menuValinta = 0;

    do {
        printf("\nValitse toiminto kirjoittamalla sitä vastaava numero ja painamalla ENTER\n\n");
        printf("1. Näytä saldo näytöllä\n2. Näytä saldo kuitilla\n\n");
        printf("Valintasi --> ");
        menuValinta=lueKokonaisluku();

        switch (menuValinta) {
            case 1:
                printf("Tililläsi on %.2lf euroa\n\n", *tilinSaldo);
                vaaraValintaMenussa = 0;
                break;
            case 2:
                printf("TULOSTETTU KUITTI\n\n Tililläsi on %.2lf euroa\n\nTULOSTETTU KUITTI\n\n", *tilinSaldo);
                vaaraValintaMenussa = 0;
                break;
            default:
                printf("Valintaasi vastaavaa toimintoa ei löytynyt. Yritä uudelleen.\n\n\n");
                vaaraValintaMenussa = 1;
                break;
        }
    } while (vaaraValintaMenussa==1);
}

/*
 Funktio rahan tallettamiselle. Käyttäjä voi tallettaa vapaavalintaisen summan rahaa tililleen. Funktio tulostaa kehotteen, jonka jälkeen
 käyttäjältä luetaan double-tyyppiseen muuttujaan talletettava summa (tilille voi tallettaa myös eurosenttejä). Optiona lopuksi
 on näyttää saldo, joka aukaisee saldonnäyttö.c -moduulin.

 Parametrit:
 -tilinSaldo (double): main-funktiolta saatu tilin saldo

 Ei paluuarvoja.

*/

void rahanTalletus (double *tilinSaldo, char * PIN_koodi_kortti, char *kayttajanTiliNro) {

    double talletettuSumma;
    int saldonNayttoNostonJalkeen;
    int vaaraValintaMenussa = 0;

    talletettuSumma=lueReaaliluku();

    *tilinSaldo=*tilinSaldo+talletettuSumma;
    updateSaldo(tilinSaldo,PIN_koodi_kortti,kayttajanTiliNro);

    printf("\nHaluatko katsoa tilin saldon talletuksen jälkeen?\n\n");

    do {
        printf("1. Kyllä\n2. Ei\n\n");
        printf("Valintasi --> ");
        saldonNayttoNostonJalkeen=lueKokonaisluku();
        if (saldonNayttoNostonJalkeen == 1) {
            saldonNaytto(tilinSaldo);
            vaaraValintaMenussa=0;
        } else if (saldonNayttoNostonJalkeen==2) {
            vaaraValintaMenussa=0;
        } else {
            vaaraValintaMenussa=1;
            printf("Valintasi oli virheellinen. Yritä uudelleen.\n");
        }
    } while (vaaraValintaMenussa==1);
}
int callback(void *NotUsed, int argc, char **argv,
             char **azColName) {

    NotUsed = 0;

    for (int i = 0; i < argc; i++) {

        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }

    printf("\n");

    return 0;
}