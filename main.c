// main-funktiossa kysytään käyttäjän tilinumero. Jos tilinumeroa vastaava tilitiedosto löytyy, luetaan tiedostosta tilin PIN-koodi ja
// käyttäjän syöttämää PIN-koodia verrataan siihen. Oikea vastaus aukaisee päävalikkofunktion. Päävalikosta löytyvät optiot rahan
// nostoon, saldon katsomiseen ja uloskirjautumiseen. Jos käyttäjä antaa vääränlaisen syötteen, palataan päävalikon alkuun. Optioita
// vastaavat numerot aukaisevat niitä vastaavat funktiot.

#include <stdio.h>
#include <search.h>
#include <string.h>
#include <term>
#include "header.h"
#include "sqlite3.h"

int main(void) {

    char PIN_koodi_kortti[MAX_BUF_SZ];
    char PIN_koodi_syotto[MAX_BUF_SZ];
    char kayttajanTiliNro[MAX_BUF_SZ];
    int i=0;
    double tilinSaldo;
    double *saldoPtr;
    saldoPtr=&tilinSaldo;
    FILE *tiliTiedosto;

    // SQLITE TESTING AREA STARTS HERE SQLITE TESTING AREA STARTS HERE SQLITE TESTING AREA STARTS HERE SQLITE TESTING AREA STARTS HERE SQLITE TESTING AREA STARTS HERE SQLITE TESTING AREA STARTS HERE

    //VARIABLES AND CHAR ARRAYS RESERVED FOR SQLITE-RELATED BUSINESS
    sqlite3 *db;
    int returnCode;
    char *errMsg;
    char *sql;
    char dbTesti[256];
    int userInput=1;

    returnCode = sqlite3_open("bankdata.db",&db); // OPEN THE DATABASE CONNECTION

    if (returnCode!=SQLITE_OK) { // IF DATABASE CONNECTION FAILS, PRINT AN ERROR MESSAGE AND EXIT THE PROGRAM. ELSE, PRINT SUCCESS MESSAGE AND CONTINUE.
        fprintf(stdout, "Can't open the database: %s\n", sqlite3_errmsg(db));
        return 0;
    } else {
        fprintf(stdout, "Database opened successfully.\n");
    }

    sql = "PRAGMA foreign_keys = 1;" // ENFORCE FOREIGN KEYS AND CREATE TABLES IF THEY DONT EXIST
          "CREATE TABLE IF NOT EXISTS customers(Id INTEGER PRIMARY KEY AUTOINCREMENT, Firstname TEXT, Lastname TEXT, Pincode INT);"
          "CREATE TABLE IF NOT EXISTS accounts(Accountnumber INTEGER PRIMARY KEY AUTOINCREMENT, Owner INT NOT NULL, Balance INT, FOREIGN KEY(Owner) REFERENCES customers(Id));";

    returnCode=sqlite3_exec(db,sql,callback,0,&errMsg);

    if( returnCode!= SQLITE_OK ){ // IF ANY OF THE THREE SQL STATEMENTS ABOVE CAUSES UNDESIRABLE CONSEQUENCES, PRINT AN ERROR MESSAGE AND EXIT. ELSE, PRINT SUCCESS MESSAGE AND CONTINUE.
        fprintf(stdout, "SQL error: %s\n", errMsg);
        sqlite3_free(errMsg);
        return 0;
    } else {
        fprintf(stdout, "Operation successful.\n");
    }

    sql = "INSERT INTO customers (Firstname, Lastname, Pincode) VALUES ('Test', 'User', 1234);" // INSERT TEST USERS AND ACCOUNTS IN THE TABLES.
          "INSERT INTO accounts (Owner, Balance) VALUES (1, 12000);";

    returnCode=sqlite3_exec(db,sql,callback,0,&errMsg);

    if( returnCode!= SQLITE_OK ){ // IF SOME OF THE TWO SQL STATEMENTS ABOVE CAUSES UNDESIRABLE CONSEQUENCES, PRINT AN ERROR MESSAGE AND EXIT. ELSE, PRINT SUCCESS MESSAGE AND CONTINUE.
        fprintf(stdout, "SQL error: %s\n", errMsg);
        sqlite3_free(errMsg);
        return 0;
    } else {
        fprintf(stdout, "Operation successful.\n");
    }

    printf("Give your account number > "); // ASK USER'S ACCOUNT NUMBER
    userInput=lueKokonaisluku();

    snprintf(dbTesti,256,"SELECT Accountnumber, Balance FROM accounts WHERE Owner=%d;",userInput); // INSERT USER INPUT TO THE SQL STATEMENT

    returnCode=sqlite3_exec(db,dbTesti,callback,0,&errMsg);

    if( returnCode!= SQLITE_OK ){ // IF THE SQL STATEMENT ABOVE CAUSES UNDESIRABLE CONSEQUENCES, PRINT AN ERROR MESSAGE AND EXIT. ELSE, PRINT SUCCESS MESSAGE AND CONTINUE.
        fprintf(stdout, "SQL error: %s\n", errMsg);
        sqlite3_free(errMsg);
        return 0;
    } else {
        fprintf(stdout, "Operation successful.\n");
    }

    sqlite3_close(db); // CLOSE THE DATABASE CONNECTION

    // SQLITE TESTING AREA ENDS HERE SQLITE TESTING AREA ENDS HERE SQLITE TESTING AREA ENDS HERE SQLITE TESTING AREA ENDS HERE SQLITE TESTING AREA ENDS HERE SQLITE TESTING AREA ENDS HERE

    do {
        for (i=0;i<=MAX_BUF_SZ;i++) {
            PIN_koodi_kortti[i]='\0';
            PIN_koodi_syotto[i]='\0';
            kayttajanTiliNro[i]='\0';
        }

        printf("Tervetuloa pankkiautomaatille! Anna tilinumerosi (ps. se on 12345 TAI 00000) --> ");

        fgets(kayttajanTiliNro, MAX_BUF_SZ, stdin);
        if (kayttajanTiliNro[strlen(kayttajanTiliNro) - 1] == '\n') {
            kayttajanTiliNro[strlen(kayttajanTiliNro) - 1] = '\0';
        }
        strcat(kayttajanTiliNro, ".tili");

        tiliTiedosto = fopen(kayttajanTiliNro, "r");
        if (tiliTiedosto == NULL) {
            printf("Tilinumeroasi vastaavaa tiliä ei löytynyt. Poista korttisi lukijasta ja yritä uudelleen.\n");
        } else {

            printf("Anna PIN-koodi --> ");
            fgets(PIN_koodi_syotto, MAX_BUF_SZ, stdin);

            if (PIN_koodi_syotto[strlen(PIN_koodi_syotto) - 1] == '\n') {
                PIN_koodi_syotto[strlen(PIN_koodi_syotto) - 1] = '\0';
            }

            fgets(PIN_koodi_kortti, MAX_BUF_SZ, tiliTiedosto);

            if (PIN_koodi_kortti[strlen(PIN_koodi_kortti) - 1] == '\n') {
                PIN_koodi_kortti[strlen(PIN_koodi_kortti) - 1] = '\0';
            }

            if ((strcmp(PIN_koodi_kortti, PIN_koodi_syotto)) == 0) {
                printf("\nPIN-koodi oikein\n\nTervetuloa!\n");
                fscanf(tiliTiedosto, "%lf", &tilinSaldo);
                fclose(tiliTiedosto);

                valikko(saldoPtr, PIN_koodi_kortti, kayttajanTiliNro);

            } else {
                printf("\nPIN-koodi väärin. Poista kortti lukijasta ja yritä uudelleen.\n");
            }
        }
    } while (1);
}
