#define MAX_BUF_SZ 256

void rahanNosto( double *, char *, char * );
void rahanTalletus ( double *, char *, char * );
void saldonNaytto( double * );
void roskienKeruu ( void );
double lueReaaliluku( void );
int lueKokonaisluku( void );
void valikko ( double *, char *, char * );
void updateSaldo ( double *, char *, char * );
int callback(void *NotUsed, int argc, char **argv, char **azColName);