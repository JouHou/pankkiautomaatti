/* Rahannostomoduuli
 Tästä moduulista voi nostaa 20€, 40€ ja vapaavalintaisen summan (vapaavalintainen summa kysyy ensin summaa ja tarkistaa, onko se
 koostettavissa 20€ ja 50€ seteleistä. Ennen "nostotapahtumaa" tarkistetaan, ylittääkö mahdollinen nosto tilin saldon. Optiona lopuksi
 on näyttää saldo, joka aukaisee saldonnäyttö.c -moduulin.

 Parametrit:
 - n (double): main-funktiolta saatu tilin saldo

 Paluuarvo (int): tilinSaldo, joka päivitetään noston jälkeen (tilinSaldo=tilinSaldo-nostettuSumma)
*/

#include <stdio.h>
#include "header.h"

void rahanNosto (double *tilinSaldo, char * PIN_koodi_kortti, char *kayttajanTiliNro) {

    int nostoValikonValinta=0;
    int nostettuSumma=0;
    int nostettavaSumma=0;
    int saldonNayttoNostonJalkeen=0;
    int vaaraValintaMenussa = 0;
    int kaksKymppiset=0;
    int viisKymppiset=0;
    int rahaSumma;
    int vaaraSyote=0;

    do {
        printf("\nValitse nostettava summa:\n\n");
        printf("1. 20€\n2. 40€\n3. Vapaavalintainen summa (voitava koostaa 20€ ja 50€ seteleistä)\n\n");
        printf("Valintasi --> ");
        nostoValikonValinta=lueKokonaisluku();

        printf("\n");

        switch (nostoValikonValinta) {
            case 1:
                nostettavaSumma = 20;
                if ((*tilinSaldo - nostettavaSumma) >= 0) {
                    printf("Nostit juuri tililtäsi 20 euroa\n");
                    nostettuSumma = 20;
                } else {
                    printf("Tililläsi ei ole tarpeeksi rahaa");
                }
                vaaraValintaMenussa=0;
                break;
            case 2:
                nostettavaSumma = 40;
                if ((*tilinSaldo - nostettavaSumma) >= 0) {
                    printf("Nostit juuri tililtäsi 40 euroa\n");
                    nostettuSumma = 40;
                } else {
                    printf("Tililläsi ei ole tarpeeksi rahaa");
                }
                vaaraValintaMenussa=0;
                break;
            case 3:
                printf("Vapaavalintainen summa (voitava koostaa 50€:n ja 20€:n seteleistä)\n\n");

                do {
                    printf("Syötä nostettava summa --> ");
                    do {
                        rahaSumma=lueKokonaisluku();
                        vaaraSyote=0;
                        if (rahaSumma>*tilinSaldo) {
                            printf("Tililläsi ei ole tarpeeksi rahaa. Syötä pienempi summa --> ");
                        } else if (rahaSumma==30 || rahaSumma<=20 || rahaSumma%10!=0) {
                            printf("Syöttämääsi rahasummaa ei voida koostaa 20€ ja 50€ seteleistä. Yritä uudelleen --> ");
                            vaaraSyote=1;
                        }
                    } while (rahaSumma>*tilinSaldo || vaaraSyote==1);

                    while (rahaSumma != 0) {
                        if (rahaSumma == 20 || rahaSumma == 40 || rahaSumma == 60 || rahaSumma == 80) {
                            rahaSumma = rahaSumma - 20;
                            kaksKymppiset++;
                        } else {
                            rahaSumma=rahaSumma-50;
                            viisKymppiset++;
                        }
                    }
                    printf("Saat %d kpl 50€:n seteleitä ja %d kpl 20€:n seteleitä", viisKymppiset, kaksKymppiset);

                    vaaraSyote=0;
                    rahaSumma=kaksKymppiset*20+viisKymppiset*50;

                } while (vaaraSyote==1);
                nostettuSumma=rahaSumma;
                vaaraValintaMenussa=0;
                break;
            default:
                printf("Valintaasi vastaavaa toimintoa ei löytynyt. Yritä uudelleen.\n\n\n");
                vaaraValintaMenussa=1;
                break;
        }
    } while (vaaraValintaMenussa==1);

    *tilinSaldo=*tilinSaldo-nostettuSumma;
    updateSaldo(tilinSaldo,PIN_koodi_kortti,kayttajanTiliNro);
    printf("\nHaluatko katsoa tilin saldon noston jälkeen?\n\n");

    do {
        printf("1. Kyllä\n2. Ei\n\n");
        printf("Valintasi --> ");
        saldonNayttoNostonJalkeen=lueKokonaisluku();
        if (saldonNayttoNostonJalkeen == 1) {
            saldonNaytto(tilinSaldo);
            vaaraValintaMenussa=0;
        } else if (saldonNayttoNostonJalkeen==2) {
            vaaraValintaMenussa=0;
        } else {
            vaaraValintaMenussa=1;
            printf("Valintasi oli virheellinen. Yritä uudelleen.\n");
        }
    } while (vaaraValintaMenussa==1);
}